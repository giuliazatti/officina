import React from 'react';
import ReactDOM from 'react-dom';
import './style/general-structure.scss';
import './style/header.scss';
import './style/footer.scss';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
